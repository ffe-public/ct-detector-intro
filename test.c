#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "detector.h"

#define TEST_FILE "unit_tests.csv"

static void process_line(struct system_state *state, const char *line,
                         int line_no)
{
    // Code to parse the line and set the state of the inputs goes here.

    // Run the state machine
    detector_run(state);

    // Code to check the state of the outputs goes here.
}

int main(int argc, char *argv[])
{
    struct system_state state = { 0 };
    FILE *fp;

    (void)argc;
    (void)argv;

    fp = fopen(TEST_FILE, "r");
    if (fp == NULL)
    {
        perror("Unable to open "TEST_FILE);
        exit(1);
    }

    char *line = NULL;
    size_t buflen = 0;
    ssize_t len;
    int line_no = 1;

    // Read the first line
    len = getline(&line, &buflen, fp);

    // Process all the lines
    while (len != -1)
    {
        process_line(&state, line, line_no);
        len = getline(&line, &buflen, fp);
        line_no++;
    }

    fclose(fp);
    free(line);

    printf("Tests passed\n");

    return (0);
}
