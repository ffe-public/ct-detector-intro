# Definitions
MAKE=make
RM=rm
MKDIR=mkdir

OUTDIR=out
OUTFILE=$(OUTDIR)/ct-detector
CFLAGS=-g -Wextra
CFG_INC=
CFG_LIB=
CFG_OBJ=
COMMON_OBJ=$(OUTDIR)/test.o $(OUTDIR)/detector.o 
OBJ=$(COMMON_OBJ) $(CFG_OBJ)
ALL_OBJ=$(OUTDIR)/test.o $(OUTDIR)/detector.o 

COMPILE=gcc -c $(CFLAGS) -o "$(OUTDIR)/$(*F).o" $(CFG_INC) "$<"
LINK=gcc  -g -o "$(OUTFILE)" $(OBJ) $(CFG_LIB)

# Pattern rules
$(OUTDIR)/%.o : %.c
	$(COMPILE)

# Build rules
all: $(OUTFILE)

$(OUTFILE): $(OUTDIR) $(OBJ)
	$(LINK)

$(OUTDIR):
	$(MKDIR) -p "$(OUTDIR)"

# Rebuild this project
rebuild: cleanall
	@$(MAKE) -f "$(strip $(MAKEFILE_LIST))" $(MAKEFLAGS) all

# Clean this project
clean:
	$(RM) -f $(OUTFILE)
	$(RM) -f $(OBJ)

# Clean this project and all dependencies
cleanall: clean

#
# include dependencies:
#
$(OUTDIR)/test.o: test.c detector.h
$(OUTDIR)/detector.o: detector.c detector.h
